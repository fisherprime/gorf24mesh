# Setup

## Interface generation

swig -go -c++ -intgosize 32 -module goRF24Mesh nrf24l01_spi.cpp
swig -go -cgo -c++ -intgosize 32 -includeall -module goRF24Mesh goRF24Mesh.i
