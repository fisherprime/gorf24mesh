# --------------------------------------------------
cmake_minimum_required(VERSION 3.14.3)

set(MAJOR_VERION "1")
set(MINOR_VERION "0")
set(PATCH_VERION "0")
set(TWEAK_VERION "0")
string(CONCAT PROJECT_VERSION
              "${MAJOR_VERION}"
              ".${MINOR_VERION}"
              ".${PATCH_VERION}"
              ".${TWEAK_VERION}")

project(goRF24Mesh
        VERSION "${PROJECT_VERSION}" # <major>[.<minor>[.<patch>[.<tweak>]]]]
                DESCRIPTION
                "Go RF24Mesh"
        LANGUAGES CXX)

include("${CMAKE_SOURCE_DIR}/functions.cmake")

print_sys_info()

set_default_options()

find_package(SWIG 3.0 REQUIRED)
include(${SWIG_USE_FILE})

set(module_driver_dir "${external_dir}/module_drivers")
set(module_driver_external_dir "${module_driver_dir}/external")
set(SWIG_INCLUDE_DIRECTORIES
    "${PROJECT_SOURCE_DIR}/RF24Config/wiringPi"
    "${module_driver_dir}/include"
    "${module_driver_external_dir}"
    "${module_driver_external_dir}/C/include"
    "${module_driver_external_dir}/CPP/include"
    "${module_driver_external_dir}/RF24"
    "${module_driver_external_dir}/RF24/utility"
    "${module_driver_external_dir}/RF24Mesh"
    "${module_driver_external_dir}/RF24Network"
    "${module_driver_external_dir}/wiringPi"
    "${module_driver_external_dir}/wiringPi/wiringPi")

# No need to set the SWIG_FLAGS interface file property
set(CMAKE_SWIG_FLAGS "-cgo" "-intgosize" "32")
# foreach(dir IN LISTS SWIG_INCLUDE_DIRECTORIES) set(CMAKE_SWIG_FLAGS
# ${CMAKE_SWIG_FLAGS} "-I${dir}") endforeach(dir)

set(interface_file "${PROJECT_NAME}.i")
set_source_files_properties("${interface_file}"
                            PROPERTIES
                            CPLUSPLUS
                            ON
                            INCLUDE_DIRECTORIES
                            "${SWIG_INCLUDE_DIRECTORIES}")
message(STATUS "${PROJECT_SOURCE_DIR}/RF24Config/wiringPi")

swig_add_library("${PROJECT_NAME}"
                 TYPE
                 MODULE
                 LANGUAGE
                 go
                 OUTPUT_DIR
                 cppfiles
                 SOURCES
                 goRF24Mesh.i)

target_include_directories("${PROJECT_NAME}"
                           PUBLIC "${SWIG_INCLUDE_DIRECTORIES}")
