package goRF24Mesh

// #cgo CFLAGS: -I./RF24Config/wiringPi
// #cgo CFLAGS: -I./external/module_drivers/external
// #cgo CFLAGS: -I./external/module_drivers/external/C/include
// #cgo CFLAGS: -I./external/module_drivers/external/CPP/include
// #cgo CFLAGS: -I./external/module_drivers/external/RF24
// #cgo CFLAGS: -I./external/module_drivers/external/RF24Mesh
// #cgo CFLAGS: -I./external/module_drivers/external/RF24Network
// #cgo CFLAGS: -I./external/module_drivers/external/wiringPi
// #cgo CFLAGS: -I./external/module_drivers/external/wiringPi/wiringPi
// #cgo CFLAGS: -I./external/module_drivers/include
// #cgo LDFLAGS: -lrf24-bcm -lrf24network -lrf24mesh

import "C"

// This file's purpose is to guide cgo on building the package
