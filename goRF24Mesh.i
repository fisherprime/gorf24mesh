%module goRF24Mesh

// Verbatim code inclusion
%{
#include "net/nrf24l01_spi.hpp"
#include "utility/includes.h"
%}

%include "net/nrf24l01_spi.hpp"
%include "utility/includes.h"

/* // Instantiate some template declarations */
/* %rename(intList) List<int>; // Rename to a suitable identifier */
/* class List<int> { */
/* private: */
    /* int *data; */
    /* int nitems; */
    /* int maxitems; */
/* public: */
    /* List(int max); */
    /* ~List(); */
    /* void append(int obj); */
    /* int length(); */
    /* int get(int n); */
/* }; */
/*  */
/* [> Instantiate a few different versions of the template <] */
/* %template(intList) List<int>; */
/* %template(doubleList) List<double>; */

// RF24
#include "RF24.hpp"

// RF24Network
#include "RF24Network.hpp"

// RF24Mesh class
#include "RF24Mesh.hpp"

